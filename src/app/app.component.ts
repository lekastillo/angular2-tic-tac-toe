import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app/app.component.html',
  styleUrls: ['./app/app.component.css']
})

export class AppComponent implements OnInit {
  status_string =["New Game","Turn of ","DRAW!!","WINS!"];

  status: number;

  boardSize: number;

  activity_logs: Array<string> = [];

  current_letter: string = 'X';
  current_letter_class: string = 'blue';

  board: Array<any> = [];

  moves: number = 0;


  ngOnInit(): void {
    this.boardSize=4;
    this.status=0;
    // this.startGame();
  }

  startGame():void {
    this.status=1;
    this.board = [];
    this.moves = 0;
    this.current_letter= 'X';
    this.activity_logs= [];
    this.setLogActivity("Game started!");

    for(let row=0; row<this.boardSize;row++){
      let new_row: Array<any> = []
      for(let cell=0; cell<this.boardSize;cell++){
        let new_cell: Array<any> = [row, cell, ''];
        new_row.push(new_cell);
      }
      this.board.push(new_row);
    }
    // console.log(this.board);
  }

  restartGame():void {
    this.status=0;
    this.board = [];
    this.moves = 0;
    this.current_letter= 'X';
    this.activity_logs= [];
    // this.setLogActivity("Game started!");
    this.board=[];
    console.log(this.board);
  }



  setMove(letter: string, row: number, cell: number): void{
    if (this.checkCellAvailable(row,cell)==true && this.status==1) {
      this.setLogActivity(this.current_letter+" moved: row: "+String(row+1)+" cell "+String(cell+1));
      this.board[row][cell][2]=letter;
      ++this.moves;
      this.checkWinner();
      if (letter=="X" && this.status==1){
        this.current_letter="O";
        // this.status=2;
      }else if(letter=="O"  && this.status==1){
        this.current_letter="X";
        // this.status=2;
      }
    }
  }

  checkWinner(): void {

    this.checkRowsForWinner();
    this.checkColsForWinner();
    this.checkDiaForWinner();
    this.checkDrawForWinner();
  }

  checkRowsForWinner(): void {
    if (this.status==1){
      for(let row=0; row<this.boardSize; row++){
        if (this.status==3){
          break;
        }
        let cur_row: Array<any> = [];

        for(let cell=0; cell<this.boardSize;cell++){
          cur_row.push(this.board[row][cell][2]==this.current_letter ? true : false);
        }

        // console.log(cur_row);
        if (cur_row.indexOf(false) == -1){
          this.status=3;
          this.setLogActivity(this.current_letter+" wins!");
          break;
        }
      }
    }
  }

  checkColsForWinner(): void {
    if (this.status==1){
      for(let col=0; col<this.boardSize; col++){
        if (this.status==3){
          break;
        }
        let cur_col: Array<any> = [];

        for(let row=0; row<this.boardSize;row++){
          cur_col.push(this.board[row][col][2]==this.current_letter ? true : false);
        }

        // console.log(cur_col);
        if (cur_col.indexOf(false) == -1){
          this.status=3;
          this.setLogActivity(this.current_letter+" wins!");
          break;
        }
      }
    }
  }

  checkDiaForWinner(): void {
    if (this.status==1){
      let lr_dia: Array<any> = [];
      for(let dia=0; dia<this.boardSize; dia++){
        lr_dia.push(this.board[dia][dia][2]==this.current_letter ? true : false);
      }
      // console.log(lr_dia);
      if (lr_dia.indexOf(false) == -1){
        this.status=3;
        this.setLogActivity(this.current_letter+" wins!");
      }
    }
    if (this.status==1){
      let rl_dia: Array<any> = [];
      let z: number = this.boardSize-1;
      for(let dia=z; dia>-1; dia--){
        rl_dia.push(this.board[(z-dia)][dia][2]==this.current_letter ? true : false);
      }
      console.log(rl_dia);
      if (rl_dia.indexOf(false) == -1){
        this.status=3;
        this.setLogActivity(this.current_letter+" wins!");
      }
    }

  }

  checkDrawForWinner(): void {
    if (this.moves===(this.boardSize*this.boardSize) && this.status==1){
      this.status=2
      this.setLogActivity("Game Over!");
    }
  }

  checkCellAvailable(row: number, cell: number): boolean {
     if ( this.board[row][cell][2] == '') {
       return true;
     }else{
       return false;
     }
  }

  getLetterClass(): void {
    if (this.current_letter=="O"){
      this.current_letter_class="red";
    }else{
      this.current_letter_class="blue";
    }
  }

  setLogActivity(activity: string): void{
    this.activity_logs.push(activity);
  }

}
