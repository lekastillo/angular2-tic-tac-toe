import { XceroGamePage } from './app.po';

describe('xcero-game App', () => {
  let page: XceroGamePage;

  beforeEach(() => {
    page = new XceroGamePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
